INSERT INTO plugin (id, type, name, description) VALUES (90009, 1, 'rezg_oman_app_secure', 'RezG Oman Secure File Log Reader');

INSERT INTO plugin_sid (plugin_id, sid, category_id, class_id, name, priority, reliability) VALUES (90009, 0, NULL, NULL, 'rezg_oman_app_secure: Accepted publickey' ,3, 2);

INSERT INTO plugin_sid (plugin_id, sid, category_id, class_id, name, priority, reliability) VALUES (90009, 1, NULL, NULL, 'rezg_oman_app_secure: Session clossed|opened' ,3, 2);

INSERT INTO plugin_sid (plugin_id, sid, category_id, class_id, name, priority, reliability) VALUES (90009, 2, NULL, NULL, 'rezg_oman_app_secure: Disconnected' ,3, 2);

INSERT INTO plugin_sid (plugin_id, sid, category_id, class_id, name, priority, reliability) VALUES (90009, 3, NULL, NULL, 'rezg_oman_app_secure: Started session' ,3, 2);

INSERT INTO plugin_sid (plugin_id, sid, category_id, class_id, name, priority, reliability) VALUES (90009, 4, NULL, NULL, 'rezg_oman_app_secure: Sent|bounced' ,3, 2);

INSERT INTO plugin_sid (plugin_id, sid, category_id, class_id, name, priority, reliability) VALUES (90009, 5, NULL, NULL, 'rezg_oman_app_secure: Queue active' ,3, 2);

INSERT INTO plugin_sid (plugin_id, sid, category_id, class_id, name, priority, reliability) VALUES (90009, 6, NULL, NULL, 'rezg_oman_app_secure: Remove' ,3, 2);

INSERT INTO plugin_sid (plugin_id, sid, category_id, class_id, name,
priority, reliability) VALUES (90009, 9999, NULL, NULL,
'rezg_oman_app_secure: Default' ,3, 2);

