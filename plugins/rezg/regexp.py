#!/usr/bin/python
# 2008 DK @ ossim
# 2010/06 DK @ ossim: add .cfg support
#
# 2013/09 Ferran Fontcuberta @INNEVIS
#   - Match rules from .cfg in the same order as the Agent does
#   - Count and ignore null lines
#   - Fixed aliases translation, reading AV definitions
#   - Fixed "y" modifier
#   - Plugin file can have any extension
#   - Deleted "number" modifier, no clue what it does
#       (ok, is for mono-regex)
# TODO
#   - Make multi-line
#   - Fix error with null lines when mixing EOLs (win/*nix/osx);
#       maybe look for NEWLINE parameter on OPEN method
#   - Fix multiline regex: it does not match "newlines" with "."
 
import sys,re
import ConfigParser
from os.path import isfile
 
############################## Function definitions ###########################
 
def hitems(config, section):
    hash = {}
    for item in config.items(section):
        hash[item[0]] = _strip_value(item[1])
    return hash
 
def _strip_value(value):
    from string import strip
    return strip(strip(value, '"'), "'")
 
def get_entry(config, section, option):
    value = config.get(section, option)
    value = _strip_value(value)
    return value
 
def translate_aliases(regex):
    for alias in aliases:
        tmp_al = ""
        tmp_al = "\\" + alias;
        regex = regex.replace(tmp_al,aliases[alias])
    return regex
############################## End definitions ###########################
 
############################## Aliases definitions ###########################
aliases = {}
if isfile('/etc/ossim/agent/aliases.cfg'):
    try:
        aliases_file = open('/etc/ossim/agent/aliases.cfg', mode='rU')
    except Exception:
        print "[W] Aliases file can not be opened."
    else:
        for line in aliases_file.readlines():
            if line[0] in ('\s', '#', '[', '\n', ';'):
                    continue
            else:  
                    (alias_name, alias_value) = line.split('=',1)
                    alias_value = alias_value.strip()
 
                    aliases[alias_name]=alias_value
 
else:
    print "[W] Aliases file does not exist, using defaults"
    aliases['IPV4']="\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"
    aliases['IPV6_MAP']="::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"
    aliases['MAC']="\w{1,2}:\w{1,2}:\w{1,2}:\w{1,2}:\w{1,2}:\w{1,2}"
    aliases['PORT']="\d{1,5}"
    aliases['HOSTNAME']="((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)([a-zA-Z])+)"
    aliases['TIME']="\d\d:\d\d:\d\d"
    aliases['SYSLOG_DATE']="\w{3}\s+\d{1,2}\s\d\d:\d\d:\d\d"
    aliases['SYSLOG_WY_DATE']="\w+\s+\d{1,2}\s\d{4}\s\d\d:\d\d:\d\d"
############################## End definitions ###########################
 
 
 
try:
   tmp = sys.argv[3]
except:
    print "\n\t%s log_filename regexp modifier" % sys.argv[0]
    print "\n\tmodifier can be V/v/y/n/a number indicating the offset to show"
    print "\ty --> show not matched lines"
    print "\tn --> do not show not matched lines"
    print "\tnumber --> Show $number"
    print "\tv --> verbose, show matching line"
    print "\tV --> vverbose, show matching line and regexp"
    print "\tq --> quiet, just show a summary"
    print "\tIf regexp is a plugin file all regexps in that file will be checked\n"
    sys.exit()
 
try:
    f = open(sys.argv[1], mode='rU')
except Exception:
    print "[E] Log file cannot be opened."
    sys.exit(-1)
 
if sys.argv[3] not in ('y','n','v','V','q'):
    print "[E] Modifier not found."
    sys.exit(-1)
 
data = f.readlines()
cfg_file=exp=sys.argv[2]
single_regexp=True
if isfile(cfg_file):
    single_regexp=False
    print "Multiple regexp mode used, parsing %s " % exp
else:
    if exp.endswith(".cfg") or exp.endswith(".cfg.local"):
        print "[E] Plugin file does not exist."
        sys.exit(-1)
 
line_match = 0
 
matched = 0
nulls = 0
 
if single_regexp == True:
    # single regexp mode
    multiline = False
    for line_index in range(0, len(data)):
        line = data[line_index]
        if multiline:
            if line_index != new_line_index:
                continue
            else:
                multiline = False
 
        if line == '\n':
            nulls += 1
            continue
        if exp.find('\\n') != -1 and re.search( "^"+exp.split('\\n')[0], line, re.S):
            multiline = True
            exp = exp.rstrip('\\n')
            multiline_index = exp.count('\\n')
            for a in range(1, multiline_index+1):
                line += data[line_index+a]
            line = line.rstrip('\n')
            exp = exp.replace('\\n', '\n')
            new_line_index = line_index + multiline_index + 1
 
        exp = translate_aliases(exp)
        result = re.findall(exp,line)
        try:
            tmp = result[0]
        except IndexError:
            if sys.argv[3] is "y":
                print "Not matched:", line
            continue
        # Matched
        if sys.argv[3] is "v":
            print line.replace('\n', '\\n')
        if sys.argv[3] is "V":
            print "Regexp: ", exp.replace('\n', '\\n')
            print "Line: ", line.replace('\n', '\\n')
        try:
            if int(sys.argv[3]) > 0:
                print "Match $%d: %s" % (int(sys.argv[3]),tmp[int(sys.argv[3])-1])
                #print "Match %d: %s" % (int(sys.argv[3]),result[int(sys.argv[3])])
            else:
                if sys.argv[3] is not "q":
                    print "Result: ", result
        except ValueError:
            if sys.argv[3] is not "q":
                print "Result: ", result
        matched += 1
 
    print "Counted", len(data), "lines."
    print "Matched", matched, "lines."
else:
    SECTIONS_NOT_RULES = ["config", "info", "translation"]
    rules = {}
    sorted_rules = {}
    rule_stats = []
    # .cfg file mode
    config = ConfigParser.RawConfigParser()
    config.read(cfg_file)
    for section in config.sections():
        if section.lower() not in SECTIONS_NOT_RULES :
            rules[section] = hitems(config,section)
    keys = rules.keys()
    keys.sort()
    multiline = False
    for line_index in range(0,len(data)):
        line = data[line_index]
        if multiline:
            if line_index != new_line_index:
                continue
            else:
                multiline = False
        if line == '\n':
            nulls += 1
            continue
        line_errors = 0
        for rule in sorted(rules.iterkeys()):
            rulename = rule
            regexp = get_entry(config, rule, 'regexp')
            if regexp is "":
                continue
            #elif regexp.find('\\n') != -1 and line.startswith( regexp.split('\\n')[0] ):
            elif regexp.find('\\n') != -1 and re.search( "^"+exp.split('\\n')[0], line, re.S):
                multiline = True
                regexp = regexp.rstrip('\\n')
                multiline_index = regexp.count('\\n')
                for a in range(1, multiline_index+1):
                    line += data[line_index+a]
                line = line.rstrip('\n')
                regexp = regexp.replace('\\n', '\n')
                new_line_index = line_index + multiline_index + 1
 
            # Replace vars
            regexp = translate_aliases(regexp)
            result = re.findall(regexp,line)
            try:
                tmp = result[0]
            except IndexError:
                line_errors += 1
                continue
            # Matched
            if sys.argv[3] is not 'y':
                if sys.argv[3] is not "q":
                    print
                    print "Matched using %s" % (rulename)
                if sys.argv[3] is "v":
                    print line.replace('\n', '\\n')
                if sys.argv[3] is "V":
                    print regexp.replace('\n', '\\n')
                    print line.replace('\n', '\\n')
                try:
                    if int(sys.argv[3]) > 0:
                        print "Match $%d: %s" % (int(sys.argv[3]),tmp[int(sys.argv[3])-1])
                    else:
                        if sys.argv[3] is not "q":
                            print result
                except ValueError:
                    if sys.argv[3] is not "q":
                        print result
            # Do not match more rules for this line
            rule_stats.append(str(rulename))
            matched += 1
            break
        if sys.argv[3] is 'y' and line_errors is len(rules.keys()):
            print line
 
    print "-----------------------------------------------------------------------------"
 
    for key in keys:
        print "Rule: \t%s\n\t\t\t\t\t\tMatched %d times" % (str(key), rule_stats.count(str(key)))
 
    print "Counted", len(data), "lines."
    print "Matched", matched, "lines."
    print "Ignored", nulls, "blank lines."
 
 
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4: