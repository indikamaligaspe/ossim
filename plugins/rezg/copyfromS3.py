import os

from subprocess import call

rootdir = '/workspace/securematic/OSSIM/plugins/rezg/test'
log_dir = '/workspace/securematic/OSSIM/plugins/rezg/var'

dest_file_names = ['secure.log','messages.log','mail.log']
dest_subdirs = ['app','web']
dest_file_dict = {}

# calling AWS s3 download
call(["ls", log_dir,"-l"])

#removes dest logs before starting process
for dest_sub_dir in dest_subdirs:
    for dest_file_name in dest_file_names:
        base_file_name = os.path.join(log_dir,dest_sub_dir,dest_file_name)
        base_file = open(base_file_name,"w")
        base_file.seek(0)
        base_file.truncate()
        dest_file_dict[base_file_name] = base_file

for subdir,dirs,files in os.walk(rootdir):
    for file in files:
        for dest_sub_dir in dest_subdirs:
            if dest_sub_dir in subdir:
                # print dest_sub_dir+' - '+subdir
                for dest_file_name in dest_file_names:
                    if (dest_file_name == file):
                        base_file_name = os.path.join(log_dir,dest_sub_dir,dest_file_name)
                        print dest_file_name+' - '+os.path.join(subdir,file)
                        print 'base_file_name - ' +base_file_name
                        temp_file = open(os.path.join(subdir,file))
                        dest_file_dict[base_file_name].write(temp_file.read())
                        temp_file.close();
